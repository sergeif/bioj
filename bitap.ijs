coclass 'bitapalg'

NB. Helpers
bs =: _1&(|.!.0)                   NB. bitshift
a0 =: >@:(0&{)                     NB. get first elem of t0 ; t1 ; t2...
a1 =: >@:(1&{)
a2 =: >@:(2&{)
a3 =: >@:(3&{)

NB. r amnd (k ; j ; c0 ; c1) = r^k_j = if (0=k) c0 else c1
amnd =: a2@:]`(a3@:])@.(0&~:@:a0@:])`(<@:(a0 , a1)@:])`[}

NB. x next (k ; j ; r ; h) = if (#x=j) (k+1);1;r;h else k;(j+1);r;h
next =: (a0 ; (1: + a1) ; a2 ; a3)@:]`(((1: + a0) ; 1: ; a2 ; a3)@:])@.(a1@:] = #@:[)

NB. y[x]
elem =: (<@:[) { ]

NB. p q cnd k j r h = if (k=q+1) 0 elseif (1=j) r^{k-1}_{-1}[-1] else r^k_{j-1}[-1]
cnd =: 0:`((((_1:+a0),_1:,_1:) elem a2)@:])`(((a0,(_1:+a1),_1:) elem a2)@:]) @. ((1:+(1:~:a1@:])) * ((a0@:])~:(1:+a1@:[)))

NB. 
f =: 4 : 0 NB. 
 'k j r h' =. y
 'p q' =. x
 sx =. p ~: (j-1) { h                  NB. comparision vector for j-th symbol
 cur =. sx +. bs (<k,(j-1)) { r        NB. current column
 ic =. cur *. (< (-&1) k,j) { r        NB. insert
 dc =. bs (< (k-1),j) { r              NB. delete
 rc =. bs (< (-&1) k,j) { r            NB. replace
 r =. r amnd k ; j ; cur ; (ic *. dc *. rc) NB. update R^k_j
 h next k ; j ; r ; h
)

NB. Bitap algorithm for a fuzzy searching
NB. http://en.wikipedia.org/wiki/Bitap_algorithm
NB. (pattern ; maximum number of errors) bitap text = 
NB.     -1 if pattern is absent in the text (with number of errors <= maximum number of errors)
NB.     number of errors in the other case
bitap =: 4 : 0
 'p q' =. x                         NB. pattern ; max number of errors
 im =. (((2&+ @: [) , (# @: ])) $ 1:) , 1:   NB. initial matrix K * J * pattern
 'k j r h' =. x (f ^: cnd ^: _) (0 ; 1 ; ((q , # y) im p) ; y)
 if. k = q + 1 do. _1 
 elseif. 1 = j do. k-1
 elseif. do. k
 end.
)

bitap_z_ =: bitap_bitapalg_