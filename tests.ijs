load 'bitap.ijs'

NB. bitap alg tests
testData =: 5 5 $ 0
assert 0 1 0 = bs_bitapalg_ 1 0 0
assert 0 1 0 1 1 1 0 1 = 'ABACDEAB' ~: 'A'
assert (2 (<0 1) } testData) = testData amnd_bitapalg_ (0 ; 1 ; 2 ; 3)
assert (1 ; 1 ; testData ; 'ABA') = ('FOR' next_bitapalg_ (0 ; 3 ; testData ; 'ABA'))
assert 0 = ('ATC';2) bitap 'AGGTCATCATG'
assert 1 = ('CCTG';2) bitap 'CACCGTTGCA'
('bitap tests ok',CR,LF) (1!:2) 2

exit 0